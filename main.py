import pandas as pd
import math
from sklearn.model_selection import train_test_split

positiveClass, negativeClass = 1, 2

def readAndLoadData(file_name = "data/reducedDataSetIG2.txt", test_size = 0.2):
    data = pd.read_csv(file_name)
    training_data, testing_data= train_test_split(data, test_size= test_size)
    return training_data, testing_data


def calculatePriorProb(data):
    global prob_Y, prob_N, positiveClass, negativeClass
    prob_Y = len(data.loc[data.iloc[:,-1] == positiveClass])
    prob_N = len(data.loc[data.iloc[:,-1] == negativeClass])

def calculateMeanAndStandardDeviation(data):
    global positiveClass, negativeClass
    global positiveExamplesMean, positiveExamplesStd, negativeExamplesMean, negativeExamplesStd
    positiveExamples = data.loc[data.iloc[:,-1] == positiveClass]
    negativeExamples = data.loc[data.iloc[:,-1] == negativeClass]
    positiveExamplesMean, positiveExamplesStd = calculateMeanAndStdOfAllColumns(positiveExamples)
    negativeExamplesMean, negativeExamplesStd = calculateMeanAndStdOfAllColumns(negativeExamples)

def calculateMeanAndStdOfAllColumns(data):
    meanArray = []
    stArray = []
    for columnIndex in range(0, len(data.columns)-1):
        meanArray.append(data.iloc[:,columnIndex].mean())
        stArray.append(data.iloc[:,columnIndex].std())
    return meanArray, stArray

def calculateLikelihood(row, filterClass):
    global positiveClass, positiveExamplesMean, positiveExamplesStd, negativeClass, negativeExamplesMean, negativeExamplesStd
    product = 1
    for columnIndex in range(0, len(row.columns)-1):
        valueOfAttribute = row.iloc[:,columnIndex]
        mean = positiveExamplesMean[columnIndex] if filterClass == positiveClass else negativeExamplesMean[columnIndex]
        std = positiveExamplesStd[columnIndex] if filterClass == positiveClass else negativeExamplesStd[columnIndex]
        product *= calculateProbabilityOfPredictor(valueOfAttribute, mean, std)
    return product

def calculateProbabilityOfPredictor(valueOfAttribute, mean, std):
    return normpdf(valueOfAttribute, mean, std)


def normpdf(x, mean, sd):
    try:
        var = float(sd)**2
        pi = 3.1415926
        denom = (2*pi*var)**.5
        num = math.exp(-(float(x)-float(mean))**2/(2*var))
        return num/denom
    except:
        print("Exception in NormPDF: ", float(x) , mean, sd, "sd")
        return 1

def calculateAccuracy(tp, fn, fp, tn):
    try:
        accuracy = (tp + tn) / (tp + tn + fp + fn)
        return round(accuracy * 100, 2)
    except:
        print("Exception in calculateAccuracy")
        return 0

def calculatePrecision(tp, fn, fp, tn):
    try:
        precision = tn / (tn + fn)
        return round(precision * 100, 2)
    except:
        print("Exception in calculatePrecision")
        return 0

def calculateRecall(tp, fn, fp, tn):
    try:
        recall = tn / (tn + fp)
        return round(recall * 100, 2)
    except:
        print("Exception in calculateRecall")
        return 0

def predict(data):
    global positiveClass, negativeClass
    tp, fn, fp, tn = 0, 0, 0, 0
    for rowIndex in range(0, len(data)):
        row = data.iloc[rowIndex:rowIndex+1,:]
        post_prob_y = prob_Y * calculateLikelihood(row, positiveClass)
        post_prob_n = prob_N * calculateLikelihood(row, negativeClass)
        predictedOutput = 1 if post_prob_y >= post_prob_n else 2
        actualOutput = row.iloc[:,-1].values[0]
        # print(actualOutput, predictedOutput, end="", flush=True)
        if (actualOutput == 1 and predictedOutput == 1):
            tp+=1
            # print(" True Positive")
        elif (actualOutput == 1 and predictedOutput == 2):
            fn+=1
            # print(" False Negative")
        elif (actualOutput == 2 and predictedOutput == 1):
            fp+=1
            # print(" False Positive")
        else:
            tn+=1
            # print(" True Negative")
    return calculateAccuracy(tp, fn, fp, tn), calculatePrecision(tp, fn, fp, tn), calculateRecall(tp, fn, fp, tn)

kValue = 20
accuracy, precision, recall = 0, 0, 0
for rowIndex in range(0, kValue):
    print('Iteration ' , rowIndex, end="", flush=True)
    training_data, testing_data = readAndLoadData(file_name = "data/reducedDataSetGain2.txt",test_size = 0.3)
    calculatePriorProb(training_data)
    calculateMeanAndStandardDeviation(training_data)
    a,p,r = predict(testing_data)
    print(" Accuracy: ", a, " Precision: ", p, "Recall: ", r)
    accuracy += a
    precision += p
    recall +=r

print("Avg Acc", round(accuracy/kValue,2))
print("Avg Pre", round(precision/kValue,2))
print("Avg Recall", round(recall/kValue),2)

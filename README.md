# Fraud Detection Navie Bayes

Navie Bayes Implementation for Credit Card Fraud Detection

## How to run

**Prerequisites**

* Python 3.x
* pip


### Installing

**Step-1**

``` git clone  repo_path ```


**Step-2**

``` cd fraud-detection-navie-bayes ```

**Step-3**

``` pip install -r requirements.txt ```

**Step-4**

``` python main.py ```
